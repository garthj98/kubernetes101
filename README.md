# Kubertnetes 

It is a way to manage and deploy container at scale! Works like terraform but for containers, with some extra features

Includes:

- Self healing 
- Monitoring 
- Loved by the community 

## How does it work?

K8 is setup as a cluster (like ansible)

- K8 control panel 
- K8 worker nodes

Inside the nodes you have:

- Namespaces (like a vpc)
- Services (endpoints for pods, like LB)
- Pods (container + launch templates)

### Pre requisits and pre install nodes

There are different ways to get a k8 cluster. It is generally a bit tricky to set up by yourself. The following are options

- use kubespray (ansible)
- kubeadm

Other services of kubernete that are managed:

- AWS EKS
- Google GKE

Different distrobutions of k8:

- K8 offical 
- minikube 
- 

### We are going to need 

We will use Kubesppray:

https://github.com/kubernetes-sigs/kubespray

Were going to use:

- 1 k8 control panel - t3a.xlarge (30gb)
- 2 k8 worker nodes - t3a.xlarge (20gb)
- 1 ansible machine with the right access

Minimum reuremnet mentions:

- 16gb ram 
- 4 cpu

but also smaller, we will use a t3.large with 2cpu 4GiB

```bash
git clone https://github.com/kubernetes-sigs/kubespray.git
pip3 install -r requirements.txt
cp -rfp inventory/sample inventory/mycluster
## change IPS 
declare -a IPS=(172.31.29.3 172.31.24.56 172.31.21.235)
CONFIG_FILE=inventory/mycluster/hosts.yaml python3 contrib/inventory_builder/inventory.py ${IPS[@]}
ansible-playbook -i ~/kubespray/inventory/mycluster/inventory.ini --become-user=root ~/kubespray/cluster.yml -b  --private-key=~/.ssh/k8_key.pem
```

### Set up steps 

We need 4 machines:

- t3a.xlarge 
- Start these and give them appropriate tags:
  - Ansible 
  - K8 control panel 
  - K8 agent A 
  - K8 agent B

### All SG netowrking 

All machines:

- Allow cohort_9_home_IPS

Ansible will have to connect to K8 machines to setup:

- Allow ansible into thoes machines - we can just add a group that allows cohort9_home_ips

### Create and share key pairs 

In the ansible machine: 

- Create a new key pair 

```bash
$ ssh-keygen -t rsa 
```

Log in to our k8 machines:

- add/append the public key to authorized_keys file

Test step 3 and 4 by trying to login to each k8 machine va the ansible machine

### Clone Kubespray to ansible machine

`git clone https://github.com/kubernetes-sigs/kubespray.git`

Run the requiremnets.txt

`pip3 install -r requirements.txt`

### Edit inventory file in Kubespray & other stuff 

```bash
cp -rfp inventory/sample inventory/mycluster
## change IPS 
```

On the inventory file add your k8 private IPs

```YAML
### write yaml here 
```
Run other commands 
```bash
declare -a IPS=(172.31.29.3 172.31.24.56 172.31.21.235)
CONFIG_FILE=inventory/mycluster/hosts.yaml python3 contrib/inventory_builder/inventory.py ${IPS[@]}
```

### Run playbook 

ansible-playbook -i ~/kubespray/inventory/mycluster/inventory.ini --become-user=root ~/kubespray/cluster.yml -b  --private-key=~/.ssh/k8_key.pem